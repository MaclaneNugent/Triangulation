//
//  ViewController.swift
//  Tri
//
//  Created by Number Two on 4/11/18.
//  Copyright © 2018 Senecio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var p1Lat: UITextField!
    @IBOutlet weak var p2Lat: UITextField!
    @IBOutlet weak var p3Lat: UITextField!
    @IBOutlet weak var p1Lng: UITextField!
    @IBOutlet weak var p2Lng: UITextField!
    @IBOutlet weak var p3Lng: UITextField!
    @IBOutlet weak var r1: UITextField!
    @IBOutlet weak var r2: UITextField!
    @IBOutlet weak var r3: UITextField!
    @IBOutlet weak var a: UILabel!
    @IBOutlet weak var b: UILabel!
    @IBOutlet weak var d: UILabel!
    @IBOutlet weak var iBar: UILabel!
    @IBOutlet weak var jBar: UILabel!
    @IBOutlet weak var sBar: UILabel!
    
    struct p {
        var lat: Double = 0.0
        var lng: Double = 0.0
        var z: Double = 5
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        p1Lat.delegate = self
        p2Lat.delegate = self
        p3Lat.delegate = self
        p1Lng.delegate = self
        p2Lng.delegate = self
        p3Lng.delegate = self
        r1.delegate = self
        r2.delegate = self
        r3.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func calculate(_ sender: Any) {
        //sBar.text = "\(p1.lat)"
        
        var p1 = p()
        var p2 = p()
        var p3 = p()
        var r1Dis: Double = Double(r1.text!)!
        var r2Dis: Double = Double(r2.text!)!
        var r3Dis: Double = Double(r3.text!)!
        
        p1.lat = Double(p1Lat.text!)!
        p1.lng = Double(p1Lng.text!)!
        p2.lat = Double(p2Lat.text!)!
        p2.lng = Double(p2Lng.text!)!
        p3.lat = Double(p3Lat.text!)!
        p3.lng = Double(p3Lng.text!)!
        
        
        
        
    }
    
    func calculateI(p1 :p, p2: p) -> p {
        var numerator = p()
        var demoninator = p()
        var store = p()
        
        numerator = vectorSubtraction(p1: p1, p2: p2)
        demoninator.lat = abs(numerator.lat)
        demoninator.lng = abs(numerator.lng)
        demoninator.z = abs(numerator.z)
        
        store = vectorDivision(p1: numerator, p2: demoninator)
        
        return store
    }
    
    func vectorSubtraction(p1 :p, p2 :p) -> p {
        var i = p()
        
        i.lat = p2.lat - p1.lat
        i.lng = p2.lng - p1.lng
        i.z = p2.z - p1.z
        
        return i
    }
    
    
    func vectorDivision(p1 :p, p2 :p) -> p {
        var i = p()
        
        
        
        return i
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        resignFirstResponder()
    }
}

extension ViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

